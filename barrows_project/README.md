Barrows Project Management System
Installation
1. git clone https://gitlab.com/brianmafu/barrows.git
2. Create virtual environment: eg. django-1.6.5
3. Activate virtual environment: workon django-whatever
4. cd into barrows/barrows_project
5. Install Packages: pip install -r requirements.txt
6. Run Syncdb ( Defaults to using sqlite3 database): python manage.py syncdb --no-initial
7. Syncdb command will  create a Default 'Super User': Follow the prompts
8. Run: python manage.py collectstatic --noinput for static assets
9. Start Up App Server for Project: python manage.py runserver 0.0.0.0:portnumber
10. Go Browser and enter address and port number specified in Step 6
11. Log in with the username/password you created in step 6.