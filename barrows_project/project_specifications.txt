Write an application in Python (2.7) using Django (1.6.5) that functions as a CRM at a company, with the following requirements for the system:


A login that prevents access to any of the pages without being logged in.

The system fetches a list of Clients from a database (preferably SQLite) and displays them in a table on a different page marked as /clients e.g. http://127.0.0.1:800/clients

The Clients must have the following fields: Client Name, Contact Person, Contact Number.

In addition the system will also be required to add a Project. Project's have the following fields: Project Name, Project Status (Active, Inactive, Complete).

Projects are logged against a Client and a Client can have one or more Projects logged against it.

The system must be able to Add/Edit/Delete a Client and a Project.

Ideally the Add/Edit/Delete must be accomplished using AJAX to avoid page reloads on form submit. 

GUI is entirely up to you, style points awarded.


The code should be clean and commented suitably throughout and the system should be well documented, indicating how to execute the program. Also the environment of python must also be included (ie: use pip freeze) if any extra packages are used.