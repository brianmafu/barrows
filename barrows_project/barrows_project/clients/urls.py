# coding: utf-8

from django.conf.urls import patterns, include, url

from views import *
# Client App Urls
urlpatterns = [
	url(r'^$', all_clients, name='all_clients'),
    url(r'^add/$', add_client, name='add_client'),
    url(r'^add-form/$', render_add_client_form, name='render_add_client_form'),
    url(r'^edit-form/(?P<id>\d+)/$', render_edit_client_form, name='render_edit_client_form'),
    url(r'^edit/(?P<id>\d+)/$', edit_client, name='edit_client'),
    url(r'^delete/(?P<id>\d+)/$', delete_client, name='delete_client'),
]