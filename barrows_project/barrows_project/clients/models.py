# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your model
# Client Model
class Client(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)
    client_name = models.CharField(max_length=50, unique=True, blank=False, null=False, editable=False)
    contact_name = models.CharField(max_length=50, blank=True)
    contact_number = models.CharField(max_length=10)

    def __str__(self):
    	return self.client_name