from django import forms
from models import Client

# Client Form
class ClientForm(forms.ModelForm):
    
    client_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), 
        max_length=50,
        required=True)
    contact_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), 
        max_length=50,
        required=False)
    contact_number = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), 
        max_length=50,
        required=False)
    

    class Meta:
        model = Client
        fields = ['client_name', 'contact_name', 'contact_number']