# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from barrows_project.clients.models import Client

PROJECT_STATUS = ['Active', 'InActive', 'Complete']

# Project Model
class Project(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)
    project_name = models.CharField(max_length=50, unique=True, editable=False)
    project_status = models.CharField(max_length=50, blank=True, choices=zip(PROJECT_STATUS, PROJECT_STATUS))
    client = models.ForeignKey(Client)

    def __str__(self):
    	return self.project_name
