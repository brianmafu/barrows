# coding: utf-8

from django.conf.urls import patterns, include, url

from views import *

# Project App Urls
urlpatterns = [
	url(r'^$', all_projects, name='all_projects'),
    url(r'^add/$', add_project, name='add_project'),
    url(r'^add-form/$', render_add_project_form, name='render_add_project_form'),
    url(r'^edit-form/(?P<id>\d+)/$', render_edit_project_form, name='render_edit_project_form'),
    url(r'^edit/(?P<id>\d+)/$', edit_project, name='edit_project'),
    url(r'^delete/(?P<id>\d+)/$', delete_project, name='delete_project'),
]