$(function () {
  $(".publish").click(function () {
    $.ajax({
      url: '/projects/add/',
      data: $("form").serialize(),
      cache: false,
      type: 'post',
      success: function (data) {
         $(".errorlist").show();
        if (data.confirm_msg == null)
        {         
          $.each(data, function(i, el){
            strElement = "#" +"id_" + el.element + "_errors"
            $( strElement ).html(this.error_message);
            $( strElement ).addClass("error");
          });
        }
        else
        {
          var confirm_msg = data.confirm_msg
        $('#id_confirmation_p').html("<strong>"+confirm_msg+"</strong>");
        $.fn.prettyPhoto({modal: true});
        $.prettyPhoto.open('#id_confirmation_p');
          
        }
      },
      error: function(data) {
        alert("Something went wrong!");  
       
      }
    });
  });

  $(".publish-edit").click(function () {
    $.ajax({
      url: '/projects/edit/'+$("#id_edit_project_id").val()+"/",
      data: $("form").serialize(),
      cache: false,
      type: 'post',
      success: function (data) {
         $(".errorlist").show();
        if (data.confirm_msg == null)
        {         
          $.each(data, function(i, el){
            strElement = "#" +"id_" + el.element + "_errors"
            $( strElement ).html(this.error_message);
            $( strElement ).addClass("error");
          });
        }
        else
        {
          var confirm_msg = data.confirm_msg
          $('#id_confirmation_p').html("<strong>"+confirm_msg+"</strong>");
          $.fn.prettyPhoto({modal: true});
          $.prettyPhoto.open('#id_confirmation_p');
          
        }
      },
      error: function(data) {
        alert("Something went wrong!");  
       
      }
    });
  }); 
  
});

function onclick_delete_anchor(i)
{   
    
    var element_id = "id_" + i;
    $("#"+element_id).remove();
    $.ajax({
      url: '/projects/delete/' + i + "/",      
      cache: false, 
      success: function (data) {        
        $('#id_confirmation_p').html("<strong>"+data.confirm_msg+"</strong>");
        $.fn.prettyPhoto({modal: true});
        $.prettyPhoto.open('#id_confirmation_p');
          
      },
      error: function(data) {
        alert("Something went wrong!");  
       
      }
    });
}